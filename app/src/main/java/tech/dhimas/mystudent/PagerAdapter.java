package tech.dhimas.mystudent;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int mTabs;

    public PagerAdapter(FragmentManager fm, int mTabs) {
        super(fm);
        this.mTabs = mTabs;
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {
            case 0: {
                ProfileTabFragment profileTab = new ProfileTabFragment();
                return  profileTab;
            }
            case 1: {
                SettingsTabFragment settingsTab = new SettingsTabFragment();
                return settingsTab;
            }
        }

        return null;
    }

    @Override
    public int getCount() {
        return mTabs;
    }
}
